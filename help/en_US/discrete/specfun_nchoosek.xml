<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from specfun_nchoosek.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="specfun_nchoosek" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>specfun_nchoosek</refname><refpurpose>Returns the binomial number (n,k).</refpurpose>
  </refnamediv>



<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>n :</term>
      <listitem><para> a matrix of floating point integers, must be positive</para></listitem></varlistentry>
   <varlistentry><term>k :</term>
      <listitem><para> a matrix of floating point integers, must be positive</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a matrix of floating point integers</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   b = specfun_nchoosek ( n , k )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
   <para>
Computes the number of k-element subsets of an n-element set.
It is mathematically defined by
   </para>
   <para>
<latex>
\begin{eqnarray}
\frac{n!}{k! (n-k)!}
\end{eqnarray}
</latex>
   </para>
   <para>
which is equivalent to ( n*(n-1)*...*(n-k+1) ) / ( k*(k-1)*...*1 ).
The implementation, though, uses a robust floating point implementation,
based on the gammaln and exp functions.
   </para>
   <para>
If n &lt; 0, k &lt; 0 or k &gt; n, then an error is generated.
   </para>
   <para>
Note about Matlab compatibility.
   </para>
   <para>
The specfun_nchoosek function and the Matlab/nchoosek functions
are different.
The Matlab nchoosek function can both compute the function
value and generate the combinations.
In this module, the two features are separated and are
provided by specfun_nchoosek (the function value) and specfun_subset
(the combination generation).
We think that this is a better design.
In practice, the specfun_nchoosek can take a matrix n and a
scalar k, which cannot be done by Matlab/nchoosek.
   </para>
   <para>
Note about floating point accuracy.
   </para>
   <para>
We have factorial(170) ~ 7.e306.
So n=170 is the greatest integer for which n! can be computed.
If the naive formula b = n!/(k! (n-k)! )
was used directly, the maximum value n for which
the binomial can be computed is n = 170.
But binomial(171,1) = 1, so there is no reason
to prevent the computation of 1 because an intermediate
result is greater than 1.e308.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
c = specfun_nchoosek ( 4 , 1 ) // 4
c = specfun_nchoosek ( 5 , 0 ) // 1
c = specfun_nchoosek ( 5 , 0:5 )

// The following test shows that the
// implementation is not naive
c = specfun_nchoosek ( 10000 , 134 )
exact = 2.050083865033972676e307
relerror = abs(c-exact)/exact
// On a Windows system, we got ~ 1.e-11, which shows that the implementation
// was, in this case, accurate up to 11 digits (instead of 15-17).

// Another difficult case
c = specfun_nchoosek ( 1.e20 , 2 )
expected = 5.000D+39

// The following test shows that the implementation is vectorized.
specfun_nchoosek (10,0:10) // [1,10,45,120,210,252,210,120,45,10,1]
// The following would be different in Matlab
specfun_nchoosek (1:10,1)
// The following would be impossible in Matlab
specfun_nchoosek (1:10,1:10)

// Generates an error
c = specfun_nchoosek ( 17 , 18 )
c = specfun_nchoosek ( 17 , -1 )
c = specfun_nchoosek ( 1.5 , 0.5 )

// This generates an error, since n and k do not
// have the same size.
specfun_nchoosek (ones(4,5),ones(2,3))

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - Michael Baudin</member>
   <member>Copyright (C) 2009 - 2010 - Michael Baudin</member>
   <member>Copyright (C) 2009 - John Burkardt</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Introduction to discrete probabilities with Scilab", Michael Baudin, 2010</para>
   <para>http://bugzilla.scilab.org/show_bug.cgi?id=7589, a bug report for the lack of specfun_nchoosek function in Scilab.</para>
   <para>http://en.wikipedia.org/wiki/Binomial_coefficients</para>
   <para>http://wiki.tcl.tk/1755</para>
   <para>Boost C++ librairies, Binomial Coefficients, 2006 , 2007, 2008, 2009, 2010 John Maddock, Paul A. Bristow, Hubert Holin, Xiaogang Zhang, Bruno Lalande, Johan Råde, Gautam Sewani and Thijs van den Berg</para>
</refsection>
</refentry>
