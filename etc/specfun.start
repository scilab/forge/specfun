// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2008 - INRIA - Allan CORNET
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function root_tlbx = loadspecfunlib ()

    TOOLBOX_NAME = "specfun"
    TOOLBOX_TITLE = "Specfun"
    verbose_at_startup=%f

    mprintf("Start %s\n",TOOLBOX_TITLE);

    etc_tlbx  = get_absolute_file_path(TOOLBOX_NAME+".start");
    etc_tlbx  = getshortpathname(etc_tlbx);
    root_tlbx = strncpy( etc_tlbx, length(etc_tlbx)-length("\etc\") );

    //Load  functions library
    // =============================================================================

    if ( %t ) then
        if (verbose_at_startup) then
            mprintf("\tLoad macros\n");
        end
        pathmacros = pathconvert( root_tlbx ) + "macros" + filesep();
        specfunlib  = lib(pathmacros);
    end

    // load gateways
    // =============================================================================

    if ( %f ) then
        if (verbose_at_startup) then
            mprintf("\tLoad gateways\n");
        end
        ilib_verbose(0);
        exec( pathconvert(root_tlbx+"/sci_gateway/loader_gateway.sce",%f));
    end

    // Load and add help chapter
    // =============================================================================

    if or(getscilabmode() == ["NW";"STD"]) then
        if (verbose_at_startup) then
            mprintf("\tLoad help\n");
        end
        path_addchapter = pathconvert(root_tlbx+"/jar");
        if ( isdir(path_addchapter) <> [] ) then
            add_help_chapter("specfun", path_addchapter, %F);
        end
    end

    // add demos
    // =============================================================================

    if ( %t ) then
        if or(getscilabmode() == ["NW";"STD"]) then
            if (verbose_at_startup) then
                mprintf("\tLoad demos\n");
            end
            pathdemos = pathconvert(root_tlbx+"/demos/specfun.dem.gateway.sce",%f,%t);
            add_demo("specfun",pathdemos);
        end
    end

    // ====================================================================
    // A Welcome message.

    if (verbose_at_startup) then
        mprintf("\tType ""help Specfun Toolbox"" for quick start.\n");
        mprintf("\tType ""demo_gui()"" and search for ""specfun"" for Demonstrations.\n");
    end

    // =============================================================================
    // Create variables at level #0

    specfunlib = resume(specfunlib)

endfunction

if ( isdef("specfunlib") ) then
    warning("	Library is already loaded (""ulink(); clear specfunlib;"" to unload.)");
    return;
end

loadspecfunlib();
clear loadspecfunlib;

